import numpy as np
from matplotlib import pyplot as plt

# batch file read
# 50 6 1000 32
# 50(batchsize) x 6 (src x ux,uz) x 1000 (timeDim) x 32 (nRcv)

######################################################
# Baseline vel (3layer model)
# 2254.54272461
# 2463.34521484
# 2545.3515625
######################################################



fileName = 'prod07_sim0525-W31-0.2_velocity-models-2D_t30.dat.npy'# 6x1000x150
fileName = 'prod07_sim0048-W31-0.2_velocity-models-2D_t100.dat.npy'
fileName = 'prod07_sim0063-W31-0.2_velocity-models-2D_t150.dat.npy'
fileName = 'prod07_sim0711-W31-0.2_velocity-models-2D_t150.dat.npy'
fileName = '/projects/ml_inversion/FWI_template/test_final/aml_fwi_ext-master-flat_modifiedFCNco2_20190730_081226-250/flat_modifiedFCNco2_20190730_081226/250/test_label_10_1.npy'
fileName='/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/shear_pad2/augmentation/medium_augment_new/prebatch_data_gtruth/test_feature_10_1.npy'
fileName='/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/shear_pad2/augmentation/medium_augment_new/prebatch_data_augmented/test_predict_10_1.dat.npy'
fileName='/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/shear_pad2/augmentation/medium_augment_new/prebatch_data_gtruth/test_feature_10_1.npy'
fileName='/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/shear_pad2/augmentation/medium_augment_new/cropVel_gtruth/test_label_10_1.npy'
fileName='/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/shear_pad2/augmentation/medium_augment_new/cropVel_augmented/test_predict_10_2.npy'
baseName='/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/shear_pad2/augmentation/medium_augment_new/cropVel_augmented/test_predict_'
#img_array   = np.load('/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/vel2npy/first_500m/'+fileName)
#img_array   = np.load('/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/vel2npy/cropVel/'+fileName)
#img_array   = np.load('/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/vel2npy/second_500m/'+fileName)
#img_array   = np.load('/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/crop_vel//'+fileName)
#img_array   = np.load('/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/shear_pad2/cropVel/'+fileName)
#img_array   = np.load('/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/shear_pad2/cropVel/velocity-models-baseline-t0-2D.dat.npy')

# --- include ticks values --- #
w, h= 141, 401
s= 5
w_v= np.linspace( 0, 5* w, dtype= np.int)
h_v= np.linspace( 0, 5* h, dtype= np.int)

for j in range(10,16):
    plt.figure( figsize=(4,3))
    fileName = baseName+str(j)+'_'+'1.npy'
    img_array   = np.load(fileName)
    print(np.shape(img_array))
    print(np.max(img_array))
    print(np.min(img_array))

    print("number of masked values: ", np.count_nonzero(img_array<2400) - 17732)
#print(img_array[img_array<2200])
#print(np.sort(img_array[340]))
#print(img_array)
#print(np.shape(label_array))
## view wavefield (in)
#plt.imshow(img_array[5],cmap='gray',aspect=0.1)
    velMin = 2100.0
    velMax = 2600.0

# flipped single panel
#plt.imshow(np.fliplr(img_array[5]),cmap='gray',aspect=0.1)
#fig = plt.figure(figsize=(1,2))
#fig.add_subplot(np.shape(img_array[0])[0],np.shape(img_array[0])[1])
#fig.add_subplot(2000,150,1)
#plt.imshow(np.flip(img_array,2)[4],cmap='gray',aspect=0.1)
#plt.imshow(img_array[4],cmap='gray',aspect=0.1)
#fig.add_subplot(150,2000,2)
#plt.imshow(img_array[5],cmap='gray',aspect=0.1)
## view target (out)  velocity model
#plt.imshow(img_array.T)
    im_ratio = img_array.shape[2]/img_array.shape[1]# 1/0
## Rotated view of vel model 
## horz: z ## vert : x
#plt.imshow(label_array[2][0]-label_array[1][0],cmap='gray')

## when nodisplay
# Image of Array
    im = plt.imshow(img_array[0].T,cmap='jet',aspect=1.0   ,vmin=velMin,vmax=velMax)
    #cbar = plt.colorbar(im,fraction=0.046*im_ratio,pad=0.04)
    # cbar.set_label(' velocity (km/s) ')
    plt.yticks(np.arange(0,140,20),np.arange(0,140,20)*5)
    plt.xticks(np.arange(0,350,50),np.arange(0,350,50)*5)
#plt.yticks(np.arange(0,140,20),[0,1,2,3,4,5,6])
# Image of histogram
    '''
    plt.figure()
    plt.plot()
    plt.hist(img_array,bins='auto')
# Image of histogram
    plt.figure()
    y,x = np.histogram(img_array)
    plt.plot(x[:-1],y)
# Image of Mask
    plt.figure()
    im = plt.imshow((img_array<2400).T,cmap='jet',aspect=1.0)  # ,vmin=velMin,vmax=velMax)
#cbar = plt.colorbar(im,fraction=0.046*im_ratio,pad=0.04)
    '''

# --- modification: add labels ans ticks --- #
    #cbar.set_label(' velocity (km/s) ')
    plt.xlabel( 'Distance (m)')
    plt.ylabel( 'Depth (m)')
    #plt.xticks( h_v)
    #plt.yticks( w_v)

# show
    plt.savefig('model_'+str(j)+ '.eps', format='eps')
    plt.close("all")
    #plt.show()


