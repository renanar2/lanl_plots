clear;
clc;

open rainbow256.mat;
map = ans.c;


directory_rec  ='/projects/ml_inversion/FWI_template/smoothStart/test_Inv_12/mat/'
handle = 1500;

nx = 341;
nz = 141;

% when FWI
%nx = 401;
%nz = 231;

fnm_true = '/home/jihyun/pack-a-code/pycodes/finalPlot/test_predict_50_1.dat'
fnm_true = '/home/jihyun/pack-a-code/pycodes/finalPlot/green_60_1_tiny.dat'
fnm_true = '/home/jihyun/pack-a-code/pycodes/finalPlot/true_60_1.dat'
fnm_true='/home/jihyun/pack-a-code/pycodes/finalPlot/tiny_no_aug/noaug_60_1.dat'

fnm_true = '/home/jihyun/pack-a-code/pycodes/finalPlot/tiny_aug/test_predict_50_1.dat'
fnm_true = '/home/jihyun/pack-a-code/pycodes/finalPlot/noaug_mid_14_45.dat'
fnm_true = '/home/jihyun/pack-a-code/pycodes/finalPlot2/batch113.dat'
fnm_true = '/home/jihyun/pack-a-code/pycodes/large_fail/test_label_75_0.dat'
fnm_true='/home/jihyun/pack-a-code/pycodes/finalPlot/tiny_no_aug/noaug_60_1.dat'

% FWI result
%fnm_true  ='/projects/ml_inversion/FWI_template/test_final/Inversion/14_45/mat/iters/Inv_props950.dat'
%fid0=fopen(strcat(directory_rec, 'prod07_sim0745-W31-0.2_velocity-models-2D_t120.dat'),'r');
fid0=fopen(fnm_true,'r');
data0 = fread(fid0,'float32');
fclose(fid0);
dens0 = reshape(data0(1:3:end),nz,nx)';
pvel0 = reshape(data0(2:3:end),nz,nx)';
svel0 = reshape(data0(3:3:end),nz,nx)';

 %% bk_starting
   reset(gcf) 
   figure(handle-1), imagesc(pvel0'), 
  %figure(handle-1), imagesc(pvel0(31:371,60:201)'), 
   colormap(map);

   %% when you use colorbar
   pos = get(gca, 'Position');
  % pos(1) = 0.03;
    xoffset = -0.1;
    %pos(1) = pos(1) + 0.1; % 0.03
   set(gca, 'Position', pos)
   %cb = colorbar;
   %cb.Position = cb.Position + [0.095,0,0,0]; 
   % vel_max = max(max(pvel0));
    vel_min = min(min(pvel0));
    % fix 
    vel_max = 2550;
    vel_min = 2000;
    caxis([vel_min, vel_max]); 
    axis image; 
%% bye bye ticks
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
saveas(figure(handle-1),'largemed_fail_75_0_true.eps','epsc');