# Reads the wavefield from the .hst out file
# should pack wavefield into B W T R: batch wf 
# check the network requirement (time dim was 1000 in .npy file)

import numpy as np
import glob 
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm

# Params 4 reading binary
nrcv = 114

#inputDir = '/projects/ml_inversion/FWI_template/test_Modeling/data'
inputDir = '/projects/ml_inversion/FWI_template/test_Modeling/data2npy/'
inputDir = '/home/jihyun/data2npy/'
inputDir= '/projects/ml_inversion/FWI_template/test_Modeling_LeakSim0020/test_leakage/data2npy/'
inputDir = '/projects/ml_inversion/FWI_template/test_Modeling_Leak_newPad/data2npy/'
#fileList = glob.glob(inputDir+'/*.npy')
#testFile = fileList[20]
testFile1='/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/shear_pad2/augmentation/medium_augment_new/prebatch_data_gtruth/test_feature_10_1.npy'
testFile='/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/shear_pad2/augmentation/medium_augment_new/prebatch_data_augmented/test_predict_10_1.dat.npy'
baseFile='/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/shear_pad2/augmentation/medium_augment_new/prebatch_data_augmented/test_predict_12_'
baseFile='/projects/ml_inversion/data/CO2Model/VelMap/shear_pad/shear_pad2/augmentation/medium_augment_new/prebatch_data_augmented/test_predict'


# data=data.reshape(nrcv,-1) # nreciver 
for j in range(10,16):
    for i in range(1,15):
        print(i)
        print(baseFile+'_'+str(j)+'_'+'1'+'.dat.npy')
        data = np.load(testFile)
        data = data[i-1]
#data2 = np.load(testFile1)
#data2 = data2[0]
        print(np.shape(data))
        print(np.max(data))
#print(np.max(data2))
#data = data- data2

# check Matlab code for normalization purpose
#plt.imshow(data,cmap='gray',norm=LogNorm(),aspect=0.1)
        fig01= plt.figure( figsize=( 5, 4))
        plt.imshow(data,cmap='gray',aspect=0.1)
        plt.clim(-0.005,0.005)
        #plt.colorbar()
        #plt.xlabel('Distance (m)')
        #plt.ylabel('Depth (m)')
#plt.show()
        plt.savefig(str(j)+'_'+str(i)+'.eps', format='eps')


